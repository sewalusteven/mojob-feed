import Vue from 'vue'
import Vuex from 'vuex'
import jobs from "@/store/jobs";
import positions from "@/store/positions";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    jobs,
    positions
  }
})
