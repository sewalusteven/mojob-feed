import { baseApi } from "@/classes/api";

const state = {
    positionFunctions:[],
    response:{},
}

const getters = {
    allPositionFunctions: (state) => state.positionFunctions,
    positionFunctionResponse: (state) => state.response
}

const actions = {
    fetchPositionFunctions: ({commit}) => {
        baseApi.getPositionFunctions('https://test-api.mojob.io/public/').then((response) => {
            commit("setPositionFunctionsMutation",response);
        })
    }
}

const mutations = {
    setPositionFunctionsMutation: (state,response)=> {
        if(response.results){
            state.positionFunctions = response.results
        }else{
            //for display all results since the endpoint doesn't come with type IPage<positionFunctionListing> but just pours the results as is
            state.positionFunctions = response
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}