import { baseApi } from "@/classes/api";

const state = {
    jobs:[],
    response:{},
}

const getters = {
    allJobs: (state) => state.jobs,
    jobResponse: (state) => state.response
}

const actions = {
    fetchJobs: ({commit},{ perPage, positionFunctions }) => {
        baseApi.getJobListings('https://test-api.mojob.io/public/',perPage,positionFunctions).then((response) => {
            commit("setJobsMutation",response);
        })
    }
}

const mutations = {
    setJobsMutation: (state,response)=> {
        if(response.results){
            state.jobs = response.results
        }else{
            //for display all results since the endpoint doesn't come with type IPage<JobListing> but just pours the results as is
            state.jobs = response
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}