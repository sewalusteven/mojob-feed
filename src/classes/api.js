import axios from "axios";
export const baseApi = {
    getPositionFunctions: (url) =>
        axios.get(`${url}job/position-functions/?page_size=100`)
            .then((response) => response.data),
    getJobListings: (url,page = 5,positions = null) => {
        let usePagination = page ? 'True' : 'False';
        let perPage = page ? `&page=1&page_size=${page}`:'' //tweaking the url to cater for display all
        let positionFunctions = positions ? `&position_functions=${positions}`:''

        return axios
            .get(`${url}job/listings/?include_open=False${perPage}&use_pagination=${usePagination}${positionFunctions}`).then(
                (response) => {
                    return response.data
                }
            )
    }
}
